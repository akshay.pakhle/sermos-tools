.. _tools-date-extractor:

===============
 Date Extractor
===============

.. currentmodule: sermos_tools.catalog.date_extractor

.. automodule:: sermos_tools.catalog.date_extractor.date_extractor

.. autoclass:: sermos_tools.catalog.date_extractor.date_extractor.DateExtractor
.. autoclass:: sermos_tools.catalog.date_extractor.date_extractor.DatePatterns
