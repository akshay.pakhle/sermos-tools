.. _tools-sparse-embedding-search:

========================
 Sparse Embedding Search
========================

Coming Soon!

.. .. currentmodule: sermos_tools.catalog.embedding_search

.. .. automodule:: sermos_tools.catalog.sparse_embedding_search.sparse_embedding_search

.. .. autoclass:: sermos_tools.catalog.sparse_embedding_search.sparse_embedding_search.SparseEmbeddingSearch
..    :members:

.. .. autoclass:: sermos_tools.catalog.sparse_embedding_search.sparse_embedding_search.SimilarityResult
..    :members:
