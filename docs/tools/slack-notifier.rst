.. _tools-slack-notifier:

===============
 Slack Notifier
===============

.. currentmodule: sermos_tools.catalog.slack

.. automodule:: sermos_tools.catalog.slack_notifier.slack_notifier

.. autoclass:: sermos_tools.catalog.slack_notifier.slack_notifier.SlackNotifier
