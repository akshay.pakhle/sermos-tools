""" Test that each tool in catalog follows required conventions
"""
import os
from importlib import import_module
from sermos_tools.sermos_tools import snake_to_capital


class TestToolConventions:
    """ Test Tool Conventions
    """
    def test_file_and_class_naming_conventions(self):
        """ All tools in catalog must adhere to the following conventions:
                - Directory name must be snake_case (e.g. my_tool)
                - Directory must contain file with matching my_tool.py
                - my_tool.py must contain matching CapitalCase class (e.g. MyTool)
        """
        base_dir = 'sermos_tools/catalog/'
        dir_objects = os.listdir(base_dir)
        for dir_object in dir_objects:
            if '__' in dir_object:
                continue
            this_path = base_dir + dir_object
            if os.path.isdir(this_path):
                # By convention, there *must* be a .py file with the same name as
                # the tool's directory name which will be the entrypoint for the
                # tool.
                file_name = dir_object + '.py'
                tool_contents = os.listdir(this_path)

                assert file_name in tool_contents

                # There *must* be a class named in CapitalCase convention that
                # matches the snake_case name of the directory
                try:
                    class_name = snake_to_capital(dir_object)
                    imported_module = import_module(this_path.replace(
                        '/', '.'))
                    imported_class = getattr(imported_module, class_name)
                    assert isinstance(imported_class, type)
                    assert imported_class is not None

                except ModuleNotFoundError:
                    # This is expected in most test environments, as we very
                    # likely don't have the tool's dependencies in the global
                    # test environment. A `AttributeError` will be thrown in the
                    # event the ClassName is not found in the module.
                    pass
