#/bin/bash

export PYTHONPATH=`pwd`
echo "PYTHONPATH set to $PYTHONPATH ..."
echo "Collecting available tool extras ..."
TOOLS=$(python bin/collect_all_tools.py)

echo "Installing: $TOOLS"
pip install -e .[$TOOLS]
