#!/bin/bash

DESTINATION_BUCKET=s3://docs.sermos.ai/sermos-tools
echo "Uploading documentation to $DESTINATION_BUCKET ..."

aws s3 sync ./_docs_build $DESTINATION_BUCKET --delete
