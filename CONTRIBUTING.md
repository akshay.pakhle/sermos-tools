Contributing to Sermos Tools
============================

Sermos welcomes your contributions to help turn it into a core
component of new data science and machine learning applications!

While developers and data scientists contribute to this (and our
[related libraries](https://sermos.ai)), we want to highlight that
even non-programmers can help make Sermos more useful and successful.
Here are a few ideas for ways you can help:

   * Find a bug and report it. Even better, fix it and send in a merge
   request on GitLab!
   * If there is a feature you'd like to see, develop it!
   * Add to discussion threads around existing bugs or feature requests.
   * Help answer questions for new Sermos users on the interwebs.
   * Write an article advocating Sermos - reach out to info@sermos.ai with
   any requests for information, media assets, etc.
   * Write a HOWTO or Tutorial describing design patterns or implementation
   details you think are useful.

GIT Access
----------

Sermos Tools is developed on git, with the code hosted on GitLab.

 * [https://gitlab.com/sermos/sermos-tools](https://gitlab.com/sermos/sermos-tools)

We provide write access to anyone with proven interest in helping develop
the codebase. The process is simple: Make two contributions and request access!

Contribution Decisions
----------------------

We actively encourage modifications, tweaks, improvements, and new features
to our codebases. If you have an idea, feel empowered to code something up
even if it's at a prototype level, we believe that rapid iteration results
in the best outcome over time. Keep in mind, we generally try to adhere to
the [KISS principle](https://en.wikipedia.org/wiki/KISS_principle) and
hope you do too.

Coding Style
------------

Please ensure your submitted code is formatted using the
[YAPF Formatter](https://github.com/google/yapf). We attempt to use 
[type hints](https://docs.python.org/3/library/typing.html) most/all of the
time.

Documentation
-------------

Code needs to be documented - current and future Sermos Tools developers will really
appreciate it! Please review this
[useful guide on the subject](https://realpython.com/documenting-python-code/).

Developing
----------

TODO Add guidance here.

Testing
-------

Unit tests must pass for anything to be accepted and any new contributions
should come with tests unless clearly unnecessary.

Install the test dependencies:

    $ pip install -e .[test]

Run the tests:

    $ tox

General Guidelines for developers
----------------------------------

* If you are new, fork the Sermos Tools project
(https://gitlab.com/sermos/sermos-tools) and create a new branch for each
bug/feature you want to work on.
* When you believe your bug fix/feature is ready, create a merge request (MR).
* Maintain the coding style (indentation, variable naming, commenting, etc.)
of the existing codebase.
* Carefully explain your ideas and the changes you've made along with their
importance in the MR - this goes a long way for the maintainers' ability to
quickly understand and (hopefully) accept the MR!
* Check the "Allow commits from members who can merge to this target branch"
option while submitting the MR.
* Write informative commit messages
([nice post on the topic](https://chris.beams.io/posts/git-commit/)).
* Use full url of bug instead of mentioning just the number in messages and
discussions.
* Keep your MR current instead of creating a new one. Rebase your MR when
necessary.
* We appreciate your attention and contributions!
